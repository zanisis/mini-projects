## Mini Project
**This Project has been used:**
  - SWAPI
  - React JS
  - Redux
  - React Router

**How to Run:**
  1. *npm or yarn* install.
  2. *npm or yarn* start / run start

**Description:**
  1. user see people data list from SWAPI:
      - for see all people i using pagination.
  2. user can see detail about people with click Go Detail:
      - after user click go detail and the next page will show all detail one person in this view i limited what user see just 8 on property data from API.
  3. user can search by name people for quickly.
  4. if user want back click people.
  5. for refresh page click SWAPI on top left corner.