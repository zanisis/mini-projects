import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';
// import {withRouter} from 'react-router-dom';
import promiseMiddleware from 'redux-promise-middleware';
import {logger} from 'redux-logger';

const middleware = [thunk, promiseMiddleware(), logger];

const createStoreWithMiddleware = compose(
  applyMiddleware(...middleware)
)(createStore);

const store = createStoreWithMiddleware(rootReducer);

export default store;