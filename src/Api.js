import axios from 'axios';
import {API_BASE_URL} from './constants';

const api=(headers={})=>axios.create({
        baseURL:API_BASE_URL,
        headers
})

export default api;