import React from 'react';
import ReactDOM from 'react-dom';
// import 'bootstrap/dist/css/bootstrap.min.css';
import  './styles/css/index.css';
// import './index.css';
// import App from './App';
import registerServiceWorker from './registerServiceWorker';

import {Provider} from 'react-redux';
import store from './store';
import AppRoute from './components/WebRoute';

ReactDOM.render(
  <Provider store={store}>
    <AppRoute />
  </Provider>,
document.getElementById('main'));
registerServiceWorker();