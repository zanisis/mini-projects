import fetch from '../Api.js';
import {
  SWAPI_PEOPLE,
  DETAIL_PEOPLE,
  SEARCH_NAME
} from '../constants';
const getPeople = `/people/`
export const doFetchPeople = (num_people='', spesific=false) => ({
  type:`${spesific ? DETAIL_PEOPLE :SWAPI_PEOPLE}`,
  payload:fetch().get(`${getPeople}${!spesific ? `?page=${num_people}`:num_people}`).then(res => res.data)
})

export const searchName = (name='',page=1) => {
  return ({
    type:SEARCH_NAME,
    payload:fetch().get(`${getPeople}?search=${name}&page=${page}`).then(res => res.data)
  })
}