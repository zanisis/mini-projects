import {
  PROMISE_PENDING,
  PROMISE_FULFILLED,
  PROMISE_REJECTED,
  SWAPI_PEOPLE,
  DETAIL_PEOPLE,
  SEARCH_NAME
} from '../constants'


const  initialState = {
  peopleList: {
    isFetch: false,
    isError: null
  },
  detailPeople: {
    isFetch: false,
    isError: null
  },
  searchingPeople: {
    isError: null,
    isFetch: false
  },
  searchstr:''
};

const swarsReducers = (state=initialState, actions) => {
  switch (actions.type) {
    case `${SWAPI_PEOPLE}${PROMISE_PENDING}`: 
      return {...state, peopleList: {
        ...state.peopleList,
        isFetch: true,
        isError: null
      }}
    case `${SWAPI_PEOPLE}${PROMISE_FULFILLED}`: 
      return {...state, peopleList: {
        isFetch: false,
        isError: null,
        ...actions.payload
      }}
    case `${SWAPI_PEOPLE}${PROMISE_REJECTED}`: 
      return {...state, peopleList: {
        ...state.peopleList,
        isFetch: false,
        isError: true
      }}
    case `${DETAIL_PEOPLE}${PROMISE_PENDING}`: 
      return {...state, detailPeople: {
        ...state.peopleList,
        isFetch: true,
        isError: null
      }}
    case `${DETAIL_PEOPLE}${PROMISE_FULFILLED}`: 
      return {...state, detailPeople: {
        isFetch: false,
        isError: null,
        result: {...actions.payload}
      }}
    case `${DETAIL_PEOPLE}${PROMISE_REJECTED}`: 
      return {...state, detailPeople: {
        isFetch: false,
        isError: true
      }}
    case `${SEARCH_NAME}${PROMISE_PENDING}`: 
      return {...state, searchingPeople: {
        ...state.searchingPeople,
        isFetch: true,
        isError: null
      }}
    case `${SEARCH_NAME}${PROMISE_FULFILLED}`: 
      return {...state, searchingPeople: {
        isFetch: false,
        isError: null,
        ...actions.payload
      }}
    case `${SEARCH_NAME}${PROMISE_REJECTED}`: 
    return {...state, searchingPeople: {
      ...state.searchingPeople,
      isFetch: false,
      isError: true,
    }}
    default:
      return state
  }
};

export default swarsReducers;