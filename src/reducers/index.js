import {combineReducers} from 'redux';

import swarsReducers from './swarsReducer';

const rootReducer = combineReducers({
  dataSwars: swarsReducers
});

export default rootReducer