import React from 'react';

const NotFound = () => {
  return (
    <div>
      <h1 style={{color: 'red'}}>NOT FOUND</h1>
    </div>
  );
};

export default NotFound;