import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarBrand,
  Nav,
  Input,
  NavItem,
  NavLink } from 'reactstrap';
import classNames from 'classnames';
import {Link} from 'react-router-dom';

const Header = (props) => {
  const category = [{
    tittle_category: 'Peoples',
    classnames: classNames({'light-green': props.isActive === 'peoples'})
  }]

  return(
    <Navbar color="dark" light expand="md">
      <NavbarBrand className="yellow-light tittle-header" href="/">
        SWAPI
      </NavbarBrand>
      {/* <NavbarToggler /> */}
      <Collapse  navbar>
        <Nav navbar>
          {subNav({category, isActive: props.isActive})}
        </Nav>
        <Nav className="ml-auto" navbar>
          <NavItem>
            <Input type="name" name="name" id="name" placeholder="Search By Name" onChange={props.onSearch}/>
          </NavItem>
        </Nav>
      </Collapse>
    </Navbar>
  )
};

const subNav = (props) => {
  return (props.category.map((value, i)=>(
    <NavItem key={i} className={value.classnames}>
      <Link className="text-light nav-link" to="/">
        {value.tittle_category}
      </Link>
    </NavItem>
  )))
};

export default Header;