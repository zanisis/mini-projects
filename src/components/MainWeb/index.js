import React, {Component, Fragment} from 'react';
import {Switch} from 'react-router-dom';
import {connect} from 'react-redux';
import Route from '../WebRoute/Routes';
import Header from './Header';
import {searchName} from '../../actions/swapiAction';

// import Footer from './Footer';

class Layout extends Component {
  state = {
    isActive: 'peoples',
    searchStr: '',
    oldSearch: ''
  }

  onSearch = (e) => {
    this.setState({searchStr: e.target.value, oldSearch: this.state.searchStr})
  }

  render(){
    return(
      <Fragment>
        <Header {...this.state} onSearch={this.onSearch}/>
        <Switch>
          {
            this.props.routes.map((route, i)=>(
              <Route key={i} {...route} {...this.state}/>
            ))
          }
        </Switch>
      </Fragment>
    )
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.state.searchStr !== prevState.oldSearch) this.setState({oldSearch: this.state.searchStr})
  }
  
};

Layout.defaultProps = {
  routes: []
};

// const 

const mapDispatchToStore = dispatch => ({
  searchName: (name) => dispatch(searchName(name))
})
export default connect(null, mapDispatchToStore)(Layout)