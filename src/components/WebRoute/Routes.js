import React from 'react';
import {Route} from 'react-router-dom';

const ForSubRoutes = (route) => (
  <Route path={route.path} render={props => (
    <route.component {...props} {...route}/>
  )}/>
);

export default ForSubRoutes;