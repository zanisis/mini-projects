import React, {Fragment} from 'react';
import {
  BrowserRouter as Router
} from 'react-router-dom';

import MainWeb from '../MainWeb';
import Route from './Routes';
import PageHome from '../PageHome';
import NotFound from '../NotFound'
import People from '../People'
const routes = [
  {
    component:MainWeb,
    routes: [{
      exact: true,
      path: '/',
      component: PageHome
    },{
      exact: true,
      path: '/people/:id_people',
      component: People
    },{
      component: NotFound
    }]
  }
]

const WebRoute = () => (
  <Router>
    <Fragment>
      {routes.map((route,i)=>(
        <Route key={i} {...route}/>
      ))}
    </Fragment>
  </Router>
);

export default WebRoute;
