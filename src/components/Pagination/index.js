import React, {Component, Fragment} from 'react';

const LEFT_PAGE = 'LEFT';
const RIGHT_PAGE = 'RIGHT';

/**
 * Helper method for creating a range of numbers
 * range(1, 5) => [1, 2, 3, 4, 5]
 */
const range = (from, to, step = 1) => {
  let i = from;
  const range = [];

  while (i <= to) {
    range.push(i);
    i += step;
  }

  return range;
}

class Pagination extends Component {
  constructor(props){
    super(props)
    this.state = {
      currentPage: 1,
      status: 1
    };
  }

  totalRecords = (num) => {
    if(typeof this.props.totalRecords === 'number') return this.props.totalRecords;

    return 0;
  }

  pageLimit = (num) => {
    if(typeof this.props.pageLimit === 'number') return this.props.pageLimit;

    return 30;
  }

  pageNeighbours = (num) => {
    if(typeof this.props.pageNeighbours === 'number') return Math.max(0, Math.min(this.props.pageNeighbours, 2));

    return 0;
  }

  totalPages = (record, limit) => {
    let results = Math.ceil(record / limit)
    return results
  }

  gotoPage = page => {
    const { onPageChanged = f => f } = this.props;
    const totalPages = this.totalPages(this.totalRecords(), this.pageLimit())
    const currentPage = Math.max(0, Math.min(page, totalPages));
    const status = this.state.status * currentPage
    const paginationData = {
      currentPage,
      totalPages,
      pageLimit: this.pageLimit(),
      totalRecords: this.totalRecords()
    };

    this.setState({ currentPage, status }, () => onPageChanged(paginationData));
  }

  handleClick = page => evt => {
    evt.preventDefault();
    if(this.state.currentPage !== page) this.gotoPage(page);
  }

  handleMoveLeft = evt => {
    evt.preventDefault();
    this.gotoPage(this.state.currentPage - (this.pageNeighbours() * 2) - 1);
  }

  handleMoveRight = evt => {
    evt.preventDefault();
    this.gotoPage(this.state.currentPage + (this.pageNeighbours() * 2) + 1);
  }

  fetchPageNumbers = () => {

    const totalPages = this.totalPages(this.totalRecords(), this.pageLimit());
    const currentPage = this.state.currentPage;
    const pageNeighbours = this.pageNeighbours();
    /**
     * totalNumbers: the total page numbers to show on the control
     * totalBlocks: totalNumbers + 2 to cover for the left(<) and right(>) controls
     */
    const totalNumbers = (this.pageNeighbours() * 2) + 3;
    const totalBlocks = totalNumbers + 2;

    if (totalPages > totalBlocks) {

      const startPage = Math.max(2, currentPage - pageNeighbours);
      const endPage = Math.min(totalPages - 1, currentPage + pageNeighbours);

      let pages = range(startPage, endPage);

      /**
       * hasLeftSpill: has hidden pages to the left
       * hasRightSpill: has hidden pages to the right
       * spillOffset: number of hidden pages either to the left or to the right
       */
      const hasLeftSpill = startPage > 2;
      const hasRightSpill = (totalPages - endPage) > 1;
      const spillOffset = totalNumbers - (pages.length + 1);

      switch (true) {
        // handle: (1) < {5 6} [7] {8 9} (10)
        case (hasLeftSpill && !hasRightSpill): {
          const extraPages = range(startPage - spillOffset, startPage - 1);
          pages = [LEFT_PAGE, ...extraPages, ...pages];
          break;
        }

        // handle: (1) {2 3} [4] {5 6} > (10)
        case (!hasLeftSpill && hasRightSpill): {
          const extraPages = range(endPage + 1, endPage + spillOffset);
          pages = [...pages, ...extraPages, RIGHT_PAGE];
          break;
        }

        // handle: (1) < {4 5} [6] {7 8} > (10)
        case (hasLeftSpill && hasRightSpill):
        default: {
          pages = [LEFT_PAGE, ...pages, RIGHT_PAGE];
          break;
        }
      }

      return [1, ...pages, totalPages];

    }

    return range(1, totalPages);
  }
  
  render() {
    const totalPages = this.totalPages(this.totalRecords(), this.pageLimit());
    
    const { currentPage } = this.state;
    const pages = this.fetchPageNumbers();
    
    if (!this.totalRecords() || totalPages === 1 || !this.props.isRender || !this.props.pageLimit) return null;

    return (
      <Fragment>
        <nav aria-label="Countries Pagination">
          <ul className="pagination center">
            { pages.map((page, index) => {

              if (page === LEFT_PAGE) return (
                <li key={index} className="page-item">
                  <a className="page-link" href={null} aria-label="Previous" onClick={this.handleMoveLeft}>
                    <span aria-hidden="true">&laquo;</span>
                    <span className="sr-only">Previous</span>
                  </a>
                </li>
              );

              if (page === RIGHT_PAGE) return (
                <li key={index} className="page-item">
                  <a className="page-link" href={null} aria-label="Next" onClick={this.handleMoveRight}>
                    <span aria-hidden="true">&raquo;</span>
                    <span className="sr-only">Next</span>
                  </a>
                </li>
              );

              return (
                <li key={index} className={`page-item${ currentPage === page ? ' active' : ''}`}>
                  <a className="page-link" href={null} onClick={ this.handleClick(page) }>{ page }</a>
                </li>
              );

            }) }

          </ul>
        </nav>
      </Fragment>
    );
  }
  componentDidUpdate(prevProps, prevState) {
    if(this.props.isReset) this.setState({currentPage: 1})
  }
  
}

Pagination.defaultProps = {
  totalRecords: 87,
  pageLimit: 9,
  pageNeighbours: 2
}

export default Pagination;
