// import React from 'react';

const FormGroup = (props) => {
  if(props.isLoading) return props.loading()
  if(props.isRender) return props.render(props);

  return null
}

export default FormGroup;