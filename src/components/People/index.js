import React, { Component } from 'react';
import {Card as CardRs, CardHeader, CardBody,
  Row, Col, Container, Form, FormGroup, Label, Input} from 'reactstrap';
import {connect} from 'react-redux'
import {doFetchPeople} from '../../actions/swapiAction';

import FormGroupCustom from './FormGroup';
import RingLoader from '../Loader';

const oneOf=['name','gender','height','mass','hair_color','skin_color','eye_color','birth_year']
const capitalize = (strings) => {
  return strings.charAt(0).toUpperCase() + strings.slice(1);
}

const tittle_string = (key) => {
  switch (key) {
    case "height": return ' cm';
    case "mass": return ' kg'
    default: return ''
  }
}
class People extends Component {
  state = {
    detailPeople: {
      isFetch: false,
      result: null
    }
  }
  componentDidMount() {
    const params = this.props.match.params.id_people
    this.props.getPeople(params, true)
  }

  filterDataPeople = (people) => {

  }
  
  render() {
    const {peopleDetail} = this.props
    return (
      <div className="body section-card">
      <Container>
        <Row>
          <Col xs={12} md={{size: 6, offset: 3}}>
            <CardRs className="bg-dark br-yellow">
              <CardHeader className="yellow-light bg-deepdark">People Details</CardHeader>
              <CardBody style={{color: 'red'}}>
                <FormGroupCustom
                  isData={peopleDetail.result}
                  isRender={!peopleDetail.isFetch && peopleDetail.result && !peopleDetail.isError}
                  isLoading={peopleDetail.isFetch}
                  render={(props)=> {
                    return (
                      <Form>
                        {
                          Object.keys(props.isData).map((value, i)=>{
                            if (oneOf.indexOf(value)>=0) return(
                              <FormGroup key={i} row>
                                <Label for="Name" md={3}>{capitalize(value)}</Label>
                                <Col md={5}>
                                  <Input disabled type="name" name="name" id="name" value={`${props.isData[value]}${tittle_string(value)}`} />
                                </Col>
                              </FormGroup>
                            );

                            return null
                          })
                        }
                      </Form>
                    )
                  }}
                  loading={()=>(
                    <RingLoader 
                      className={'loading fullfilled heightred'}
                      size={100}
                    />
                  )}
                />
              </CardBody>
            </CardRs>
          </Col>
        </Row>
      </Container>
      </div>
    );
  }
};

const mapStoreToProps = ({dataSwars})=>({
  peopleList: dataSwars.peopleList,
  peopleDetail: dataSwars.detailPeople
})

const mapDispatchToStore = (dispatch)=>({
  getPeople: (num_people, spesific) => dispatch(doFetchPeople(num_people, spesific))
})

export default connect(mapStoreToProps,mapDispatchToStore)(People);