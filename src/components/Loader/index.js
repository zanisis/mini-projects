import React from 'react';
import { RingLoader } from 'react-spinners';

const Loader = (props) => {
  return (
    <div className={props.className}>
      <RingLoader
        color={props.color}
        size={props.size}
      />
    </div>
  );
};

Loader.defaultProps = {
  className:'loading fullfilled',
  color:'#FFD700',
  size:200
}

export default Loader;