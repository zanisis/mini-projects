import React, {Component} from 'react';
import { Row } from 'reactstrap';
import { connect } from 'react-redux';
// import { Link } from 'react-router-dom';

import {doFetchPeople,searchName} from '../../actions/swapiAction';
import Card from './Card';
import CardPeople from './CardPeoples';
import Pagination from '../Pagination';
import Loader from '../Loader';

// const unknown = /(?:n\/a)/i;
class PageHome extends Component {
  capitalize = (strings) => {
    return strings.charAt(0).toUpperCase() + strings.slice(1);
  }

  onPageChanged = (page) => {
    if(this.props.searchStr)
      this.props.searchName(this.props.searchStr,page.currentPage)
    else
      this.props.getPeople(page.currentPage)
  }

  render(){
    const {isActive, peopleList, searchByName,searchStr} = this.props;
    return(
      <div className="body section-card">
        <Row>
          <Card 
            isData={!searchStr ? peopleList.results : searchByName.results}
            isRender={!searchStr ? peopleList.results && isActive === 'peoples': searchByName.results}
            isLoading={peopleList.isFetch || searchByName.isFetch}
            render={(value, i)=>(
              <CardPeople key={i} {...value} />
            )}
            renderLoading={()=>(
              <Loader />
            )}
            renderError={()=>{}}
          />
        </Row>
        <Pagination
          isRender={!searchStr ? peopleList.results : searchByName.results}
          isReset={this.props.oldSearch !== this.props.searchStr}
          totalRecords={!searchStr ? peopleList.count: searchByName.count}
          pageLimit={!searchStr ? (peopleList.results ? peopleList.results.length : null) : (searchByName.results ? searchByName.results.length : null)}
          onPageChanged={this.onPageChanged}
        />
      </div>
    )
  }
  componentDidMount(){
    this.props.getPeople(1)
  }
  componentDidUpdate(prevProps, prevState) {
    if(!this.props.searchStr && prevProps.searchStr !== '') this.props.getPeople(1)
    if(this.props.searchStr && this.props.oldSearch !== this.props.searchStr) this.props.searchName(this.props.searchStr)
  }
  
};

const mapStoreToProps = ({dataSwars}) => ({
  peopleList: dataSwars.peopleList,
  searchByName: dataSwars.searchingPeople
})

const mapDispatchToProps = dispatch => ({
  getPeople: (num_people, spesific) => dispatch(doFetchPeople(num_people, spesific)),
  searchName: (name, page) => dispatch(searchName(name, page))
})

export default connect(mapStoreToProps,mapDispatchToProps)(PageHome);