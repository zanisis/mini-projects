import React from 'react';
import { Card as CardRs, Button, CardHeader, CardBody,
  CardText, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
const unknown = /(?:n\/a)/i;
const capitalize = (strings) => {
  return strings.charAt(0).toUpperCase() + strings.slice(1);
}
const CardPeople = (props) => (
  <Col {...props} sm={3} className="col-card">
    <CardRs  inverse className="black-card">
      <CardHeader className="card-header yellow">{props.name}</CardHeader>
      <CardBody className="body-section">
        <CardText>
          Gender : {!unknown.test(props.gender) ? capitalize(props.gender) : ' -'}<br/>
          Height : {props.height}cm<br/>
          Weight : {props.mass}kg<br/>
        </CardText>
        <Link to={`/people/${getNumberURL(props.url)}`}>
          <Button className="light-yellow black-ground">Go Detail</Button>
        </Link>
      </CardBody>
    </CardRs>
  </Col>
);

const getNumberURL = (url_data) => {
  const regex = /(\D)+/g
  return Number(url_data.replace(regex,''))
}
export default CardPeople