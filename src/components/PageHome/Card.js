// import React from 'react';
// import { Card as CardRs, Button, CardHeader, CardBody,
//   CardText, Row, Col } from 'reactstrap';
  
const Card = (props) => {
  if(props.isLoading) return props.renderLoading();
  if(props.isRender) return props.isData.map(props.render);
  
  return null
};

export default Card;